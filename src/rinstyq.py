#!/usr/bin/env -S python3 -B

import sys
import math
import copy
import utilyq as uq
import functools
import numpy as np
import collections


class Coords:
	x = 0.0
	y = 0.0

	def __init__(self, x, y):
		self.x = x
		self.y = y


# class Node:
#	def __init__(self, node_id = 0, node_type ='C', x = math.inf, y = math.inf, q = 0.0, prize=0.0, prize_dev=0.0, display_x=math.inf, display_y=math.inf):
#		self.node_id   = node_id
#		self.node_type = node_type  # D: depot, C: customer, S: start depot, L: linehaul customer, B: backhaul customer, T: terminal depot
#		self.x         = x
#		self.y         = y
#		self.q         = q          # demand
#		self.prize     = prize      # for robustvrpsb
#		self.prize_dev = prize_dev  # for robustvrpsb
#		self.display_x = display_x
#		self.display_y = display_y
#
#	def __str__(self):
#		return '{id: ' + '{:3d}'.format(self.node_id) + ', q: ' + \
#		'{:3f}'.format(self.q) + ', coords: [' + '{:5.1f}'.format(self.x) + \
#		', ' + '{:5.1f}'.format(self.y) + '], type: ' + self.node_type + \
#		', display_coords: [' + '{:5.1f}'.format(self.display_x) + ', ' + '{:5.1f}'.format(self.display_y) + ']}'


class Inst:
	inst_id   = ''
	comment   = ''
	inst_type = ''
	n               = 0
	num_customers   = 0
	num_depots      = 0
	Q               = 0.0
	sum_qs          = 0.0
	k               = 0
	#
	gcd_ccd = 1
	#
	smaller_cust_dem = math.inf
	larger_cust_dem = 0.0
	#
	freq_distinct_cust_dems = None # dict()
	num_distinct_cust_dems = 0
	#
	frac_lb_k = 0.0
	ceil_lb_k = 0
	#
	tight                    = None
	canonical_route_dist     = 0.0
	are_cap_n_cust_dems_ints = None
	gcd_ccd_calc             = None # gcd_of_cap_n_cust_dems_calc
	#
	discrete_Q      = 0
	discrete_sum_qs = 0
	#
	q_factor_NMAN      = 1.0 # LEGACY
	distance_NMAN      = 0.0
	service_time_NMAN  = 0.0
	edge_weight_type   = ''
	edge_weight_format = ''
	edge_data_format   = ''
	node_coord_type    = ''
	display_data_type  = ''
	node_types      = [] # D: depot, C: customer, S: start depot, L: linehaul customer, B: backhaul customer, T: terminal depot, U: unknown
	qs              = []
	discrete_qs     = []
	coords          = []
	display_coords  = []
	prize           = [] # robustvrpsb
	prize_deviation = [] # robustvrpsb
	depots          = []
	linehauls       = [] # robustvrpsb
	backhauls       = [] # robustvrpsb
	ds              = [[]]
	int_dist_measure         = True
	num_dec_places_real_dist = 0
	gamma           = 0.0 # robustvrpsb
	new_beta        = 0.0 # robustvrpsb
	split_depot     = 0   # robustvrpsb
	num_extra_verts = 0   # robustvrpsb

	accept_only_int_capacity             = True
	accept_only_zero_intern_depot_demand = True
	accept_only_int_client_demands       = True


	def __init__(self,
		instfile,
		extern_k,
		extern_rho,
		extern_q0,
		override_inst_k=True,
		override_inst_q0=True,
		real_dist=False,
		num_dec_places_real_dist=0,
		gamma=0.0,
		new_beta=0.0,
		split_depot=False,
		gcd_ccd=1,
		extern_rho_rounding_mode=None):

		self.int_dist_measure         = not real_dist
		self.num_dec_places_real_dist = num_dec_places_real_dist
		self.gamma                    = gamma
		self.new_beta                 = new_beta
		self.split_depot              = split_depot

		self.gcd_ccd = gcd_ccd

		self.recgn(instfile.read().splitlines())
		self.calc_dists()
		self.calc_intern_data()

		self.calc_and_check_extern_data(extern_k=extern_k,
			override_inst_k=override_inst_k,
			extern_rho=extern_rho,
			extern_q0=extern_q0,
			override_inst_q0=override_inst_q0,
			extern_rho_rounding_mode=extern_rho_rounding_mode)

		self.fill_discrete()
		self.adjust_display_coords()


	def recgn(self, data_lines):
		l = 0
		l = self.recgn_spec(l, data_lines)
		l = self.recgn_data(l, data_lines)
		if l != len(data_lines) and data_lines[l].strip(' \t\n') != 'EOF' and data_lines[l].strip() != '':
			uq.verb('l', l)
			uq.verb('len(data_lines)', len(data_lines))
			uq.fail('Error: recgn')


	def recgn_spec(self, l, data_lines):
		while l < len(data_lines) and data_lines[l].find(':') != -1:
			p = data_lines[l].find(':')
			tokens = [data_lines[l][:p].strip(' \t\n'), ':', data_lines[l][p+1:].strip(' \t\n')]
			# uq.verb('l', l, 'len(tokens)', len(tokens), 'tokens', tokens)
			l += 1
			t  = 0
			while t < len(tokens):
				# uq.verb('token', tokens[t])
				if   tokens[t] == 'NAME':
					t += 1
					if t + 1 < len(tokens):
						t += 1
						self.inst_id = tokens[t]
				elif tokens[t] == 'COMMENT':
					t += 1
					if t + 1 < len(tokens):
						t += 1
						self.comment = tokens[t]
					while t + 1 < len(tokens):
						t += 1
						self.comment += ' ' + tokens[t]
				elif tokens[t] == 'TYPE':
					t += 2
					self.inst_type = tokens[t]
					uq.proceed_only_if_(self.inst_type in {'', 'CVRP', 'ROBUSTVRPSB', 'TSP'}, 'Error: reading TYPE')
				elif tokens[t] == 'CAPACITY_FACTOR': # LEGACY
					t += 2
					self.q_factor_NMAN = float(tokens[t])
				elif tokens[t] == 'DIMENSION':
					t += 2
					self.n = int(tokens[t])
					if self.split_depot:
						self.num_extra_verts += 1
					self.node_types = ['C' for i in range(self.n + self.num_extra_verts)]
					self.ds         = [[0.0] * (self.n + self.num_extra_verts) for i in range(self.n + self.num_extra_verts)]
				elif tokens[t] == 'CAPACITY':
					t += 2
					self.Q = float(tokens[t])
					if self.accept_only_int_capacity:
						uq.proceed_only_if_(self.Q.is_integer(), "Error: capacity is not integer")
				elif tokens[t] in {'VEHICLES', 'VEHICLES_NMAN'}:
					t += 2
					if not self.k:
						self.k = int(tokens[t])
				elif tokens[t] in {'DISTANCE', 'DISTANCE_NMAN'}:
					t += 2
					self.distance_NMAN = float(tokens[t])
				elif tokens[t] in {'SERVICE_TIME', 'SERVICE_TIME_NMAN'}:
					t += 2
					self.service_time_NMAN = float(tokens[t])
				elif tokens[t] == 'EDGE_WEIGHT_TYPE':
					t += 2
					self.edge_weight_type = tokens[t]
					uq.proceed_only_if_(
						self.edge_weight_type in {'',
								'EUC_2D',
								'EXPLICIT',
								'FUNCTION', 'ALIEN_FUNCTION',
								'GEO'},
							'Error: reading EDGE_WEIGHT_TYPE')
				elif tokens[t] == 'EDGE_WEIGHT_FORMAT':
					t += 2
					self.edge_weight_format = tokens[t]
					uq.proceed_only_if_(
						self.edge_weight_format in {'',
								'FUNCTION',
								'LOWER_ROW',
								'EUC_2D', 'ALIEN_EUC_2D',
								'UPPER_ROW',
								'FULL_MATRIX',
								'LOWER_DIAG_ROW'},
							'Error: reading EDGE_WEIGHT_FORMAT')
				elif tokens[t] == 'EDGE_DATA_FORMAT':
					t += 2
					self.edge_data_format = tokens[t]
					uq.fail('Error: recgn_spec_key: spec key not implemented: EDGE_DATA_FORMAT : ' +  self.edge_data_format)
				elif tokens[t] == 'NODE_COORD_TYPE':
					t += 2
					self.node_coord_type = tokens[t]
					uq.proceed_only_if_(self.node_coord_type in {'',
					'TWOD_COORDS',
					'NO_COORDS',
					'GEO_COORDS', 'ALIEN_GEO_COORDS'}, 'Error: reading NODE_COORD_TYPE')
				elif tokens[t] == 'DISPLAY_DATA_TYPE':
					t += 2
					self.display_data_type = tokens[t]
					uq.proceed_only_if_(self.display_data_type in {'',
					'COORD_DISPLAY',
					'NO_DISPLAY',
					'TWOD_DISPLAY'}, 'Error: reading DISPLAY_DATA_TYPE')
				else:
					sys.exit('Error: recgn_spec')

				t += 1

		# Check and set some defaults
		if self.edge_weight_type == 'EXPLICIT':
			uq.proceed_only_if_(self.edge_weight_format != '', 'Error: checking DISPLAY_DATA_TYPE when reading EDGE_WEIGHT_TYPE')
			if self.display_data_type == '':
				self.display_data_type = 'NO_DISPLAY'

		elif self.edge_weight_type in {'FUNCTION', 'ALIEN_FUNCTION'}:
			uq.proceed_only_if_(self.edge_weight_format in  {'EUC_2D','ALIEN_EUC_2D'} and self.node_coord_type == 'TWOD_COORDS', 'Error: checking EDGE_WEIGHT_FORMAT when reading EDGE_WEIGHT_TYPE')
			self.display_data_type = 'COORD_DISPLAY'

		else: # implicit
			uq.proceed_only_if_(self.edge_weight_format in {'', 'FUNCTION'}, 'Error: checking EDGE_WEIGHT_FORMAT when reading EDGE_WEIGHT_TYPE')
			uq.proceed_only_if_(self.node_coord_type in {'', 'TWOD_COORDS', 'NO_COORDS'}, 'Error: checking NODE_COORD_TYPE when reading EDGE_WEIGHT_TYPE')

			if self.edge_weight_type == 'EUC_2D':
				self.node_coord_type = 'TWOD_COORDS'
			elif self.edge_weight_type in {'GEO_COORDS', 'ALIEN_GEO_COORDS'} and self.node_coord_type == 'NO_COORDS':
				self.node_coord_type = 'ALIEN_GEO_COORDS'

			if self.display_data_type == '':
				self.display_data_type = 'COORD_DISPLAY'

		return l


	def recgn_data(self, l, data_lines):
		uniq_err_msg = 'Error: recgn_data: trying to get edge weights again'

		is_id_valid               = True
		got_coords_or_weights     = False
		got_demands               = False
		got_depots                = False
		got_display_data          = False
		got_robustvrpsb_backhauls = False
		reached_eof_flag          = False

		# In any order
		while not reached_eof_flag and is_id_valid and l < len(data_lines):
			line = data_lines[l].strip()
			uq.verb('line ', line)
			l += 1
			if line == 'NODE_COORD_SECTION':
				uq.proceed_only_if_(self.edge_weight_type != 'EXPLICIT', 'Error: reading NODE_COORD_SECTION')
				got_coords_or_weights = uq.turn_true_only_once_or_fail(got_coords_or_weights, uniq_err_msg + ': ' + line)
				uq.verb('data_lines[', l , '] ', data_lines[l])
				l = self.recgn_node_coord_section_content(l, data_lines)

			elif line == 'EDGE_WEIGHT_SECTION':
				uq.proceed_only_if_(self.edge_weight_type == 'EXPLICIT', 'Error: reading EDGE_WEIGHT_SECTION')
				got_coords_or_weights = uq.turn_true_only_once_or_fail(got_coords_or_weights, uniq_err_msg + ': ' + line)
				l = self.recgn_edge_weight_section_content(l, data_lines)

			elif line == 'DEMAND_SECTION':
				got_demands = uq.turn_true_only_once_or_fail(got_demands, uniq_err_msg + ': ' + line)
				l = self.recgn_demand_section_content(l, data_lines)

			elif line == 'DEPOT_SECTION':
				got_depots = uq.turn_true_only_once_or_fail(got_depots, uniq_err_msg + line)
				l = self.recgn_depot_section_content(l, data_lines)

			elif line == 'DISPLAY_DATA_SECTION':
				uq.proceed_only_if_(self.display_data_type != 'NO_DISPLAY', 'Error: reading DISPLAY_DATA_SECTION')
				uq.turn_true_only_once_or_fail(got_display_data, uniq_err_msg + line)
				l = self.recgn_display_data_section_content(l, data_lines)

			elif line in {'BACKHAUL_SECTION', 'ROBUSTVRPSB_BACKHAUL_SECTION'}:
				got_robustvrpsb_backhauls = uq.turn_true_only_once_or_fail(got_robustvrpsb_backhauls, uniq_err_msg + line)
				uq.proceed_only_if_(got_depots, 'Error: recgn_data: must read depot_section before backhaul_section (robustvrpsb_backhaul_section)')
				l = self.recgn_robustvrpsb_backhauls_section_content(l, data_lines)

			elif line == 'EOF':
				reached_eof_flag = True

			elif line != '':
				uq.fail('recgn_data: identifier not recognized: ' + line)

		if self.inst_type == 'ROBUSTVRPSB' and not got_robustvrpsb_backhauls:
			uq.fail('Error: recgn_data: inst_type == ROBUSTVRPSB and not got_robustvrpsb_backhauls')

		return l


	def recgn_node_coord_section_content(self, l, data_lines):
		uq.verb('+ ' + self.recgn_node_coord_section_content.__name__)
		self.coords = [Coords(0.0, 0.0) for i in range(self.n + self.num_extra_verts)]
		check_node_id = 1
		if self.edge_weight_type in {'EUC_2D', 'GEO'}:
			for l in range(l, l + self.n):
				tokens = data_lines[l].strip(' \n').split()
				# uq.verb('tokens', tokens)
				if len(tokens) != 3:
					uq.fail('Error: recgn_node_coord_section_content: len(tokens) == ' + str(len(tokens)))
				node_id = int(tokens[0])
				uq.proceed_only_if_(node_id == check_node_id, 'node_id != check_node_id')
				check_node_id += 1
				self.coords[node_id - 1].x  = float(tokens[1])
				self.coords[node_id - 1].y  = float(tokens[2])
			l += 1
			if self.split_depot:
				self.coords[self.n] = copy.deepcopy(self.coords[0])
		elif self.edge_weight_type in {'FUNCTION', 'ALIEN_FUNCTION'} and self.edge_weight_format in {'EUC_2D', 'ALIEN_EUC_2D'} and self.node_coord_type  == 'TWOD_COORDS':
			for l in range(l, l + self.n - 1):
				tokens = data_lines[l].strip(' \n').split()
				# uq.verb('tokens', tokens)
				if len(tokens) != 3:
					uq.fail('Error: recgn_node_coord_section_content: len(tokens) == ' + str(len(tokens)))
				node_id = int(tokens[0])
				uq.proceed_only_if_(node_id == check_node_id, 'node_id != check_node_id')
				check_node_id += 1
				self.coords[node_id].x = float(tokens[1])
				self.coords[node_id].y = float(tokens[2])
			l += 1
			if self.split_depot:
				self.coords[self.n] = copy.deepcopy(self.coords[0])
		elif self.edge_weight_type == '':
			uq.fail('Error: recgn_node_coord_section_content')

		if self.display_data_type == 'COORD_DISPLAY':
			self.display_coords = copy.deepcopy(self.coords)
			self.adjust_display_coords()

		uq.verb('-' + self.recgn_node_coord_section_content.__name__)
		return l


	def recgn_edge_weight_section_content(self, l, data_lines):
		tokens = data_lines[l].strip(' \n').split()
		t  = 0
		l += 1
		if self.edge_weight_type == 'EXPLICIT':
			if self.edge_weight_format == 'LOWER_ROW':
				# LOWER_ROW: Lower triangular matrix (row-wise without diagonal entries)
				for i in range(1, self.n):
					for j in range(0, i):
						if t == len(tokens):
							tokens = data_lines[l].strip(' \n').split()
							t  = 0
							l += 1
						self.ds[i][j] = float(tokens[t])
						self.ds[j][i] = self.ds[i][j]
						t += 1
			elif self.edge_weight_format == 'LOWER_DIAG_ROW':
				# LOWER_DIAG_ROW: Lower triangular matrix (row-wise including diagonal entries)
				for i in range (0, self.n):
					for j in range(0, i + 1):
						if t == len(tokens):
							tokens = data_lines[l].strip(' \n').split()
							t  = 0
							l += 1
						self.ds[i][j] = float(tokens[t])
						self.ds[j][i] = self.ds[i][j]
						t += 1
			elif self.edge_weight_format == 'UPPER_ROW':
				# UPPER_ROW: Upper triangular matrix (row-wise without diagonal entries)
				for i in range(0, self.n - 1):
					for j in range(i + 1, self.n):
						if t == len(tokens):
							tokens = data_lines[l].strip(' \n').split()
							t  = 0
							l += 1
						self.ds[i][j] = float(tokens[t])
						self.ds[j][i] = self.ds[i][j]
						t += 1
			elif self.edge_weight_format == 'FULL_MATRIX':
				# FULL_MATRIX: Weights are given by a full matrix
				for i in range(0, self.n):
					for j in range(0, self.n):
						if t == len(tokens):
							tokens = data_lines[l].strip(' \n').split()
							t  = 0
							l += 1
						self.ds[i][j] = float(tokens[t])
						t += 1

			if self.split_depot:
				self.ds[0][self.n] = 0.0
				self.ds[self.n][0] = 0.0

		return l


	def recgn_demand_section_content(self, l, data_lines):
		self.qs = [0.0 for i in range(self.n + self.num_extra_verts)]
		check_node_id = 1
		if self.edge_weight_type == 'EUC_2D' or self.edge_weight_type == 'EXPLICIT' or self.edge_weight_type == 'GEO':
			for l in range(l, l + self.n):
				tokens = data_lines[l].strip(' \n').split()
				if len(tokens) != 2:
					uq.fail('Error: recgn_demand_section_content')
				node_id = int(tokens[0])
				uq.proceed_only_if_(node_id == check_node_id, 'node_id != check_node_id')
				check_node_id += 1
				self.qs[node_id - 1] = float(tokens[1])
				if (node_id - 1 == 0) and self.accept_only_zero_intern_depot_demand:
					uq.proceed_only_if_(self.qs[node_id - 1] == 0.0, "Error: the internal depot demand is not zero")
				if (node_id - 1 != 0) and self.accept_only_int_client_demands:
					uq.proceed_only_if_(self.qs[node_id - 1].is_integer(), "Error: a client demand is not integer")
			l += 1
		elif self.edge_weight_type in {'FUNCTION', 'ALIEN_FUNCTION'} and self.edge_weight_format in {'EUC_2D', 'ALIEN_EUC_2D'} and self.node_coord_type  == 'TWOD_COORDS':
			for l in range(l, l + self.n - 1):
				tokens = data_lines[l].strip(' \n').split()
				if len(tokens) != 2:
					uq.fail('Error: recgn_demand_section_content')
				node_id = int(tokens[0])
				uq.proceed_only_if_(node_id == check_node_id, 'node_id != check_node_id')
				check_node_id += 1
				self.qs[node_id] = float(tokens[1])
				if (node_id == 0) and self.accept_only_zero_intern_depot_demand:
					uq.proceed_only_if_(self.qs[node_id] == 0.0, "Error: the internal depot demand is not zero")
				if (node_id != 0) and self.accept_only_int_client_demands:
					uq.proceed_only_if_(self.qs[node_id - 1].is_integer(), "Error: a client demand is not integer")
			l += 1
		elif self.edge_weight_type == '':
			uq.fail('Error: recgn_demand_section_content')

		if self.split_depot:
			self.qs[self.n] = 0.0

		return l


	def recgn_depot_section_content(self, l, data_lines):
		if self.edge_weight_type in {'EUC_2D', 'EXPLICIT', 'GEO'}:
			tokens = data_lines[l].strip(' \n').split()
			uq.verb('l', l, 'len(tokens)', len(tokens), 'tokens', tokens)
			l += 1
			if len(tokens) != 1:
				uq.fail('Error: recgn_depot_section_content')
			depot = int(tokens[0])
			while depot != -1:
				self.depots.append(depot - 1)
				if self.split_depot:
					self.node_types[depot - 1] = 'S'
					self.node_types[self.n] = 'T'
				else:
					self.node_types[depot - 1] = 'D'
				tokens = data_lines[l].strip(' \n').split()
				uq.verb('l', l, 'len(tokens)', len(tokens), 'tokens', tokens)
				l += 1
				if len(tokens) != 1:
					uq.fail('Error: recgn_depot_section_content')
				depot = int(tokens[0])
		elif self.edge_weight_type in {'FUNCTION', 'ALIEN_FUNCTION'} and self.edge_weight_format in {'EUC_2D', 'ALIEN_EUC_2D'} and self.node_coord_type  == 'TWOD_COORDS':
			tokens = data_lines[l].strip(' \n').split()
			uq.verb('l', l, 'len(tokens)', len(tokens), 'tokens', tokens)
			l += 1
			if len(tokens) != 2:
				uq.fail('Error: recgn_depot_section_content')
			depot = 0
			self.depots.append(depot)
			if len(self.coords) > 0:
				self.coords[depot].x = float(tokens[0])
				self.coords[depot].y = float(tokens[1])
				if self.display_data_type != 'NO_DISPLAY':
					if len(self.display_coords) > 0:
						self.coords[depot].x = float(tokens[0])
						self.coords[depot].y = float(tokens[1])
					else:
						uq.fail('Error: recgn_depot_section_content: for this modality, the display_data_section must be read before depot_section and node_coord_section')
			else:
				uq.fail('Error: recgn_depot_section_content: for this modality, the node_coord_section must be read before depot_section')
			if self.split_depot:
				self.node_types[depot] = 'S'
				self.node_types[self.n] = 'T'
			else:
				self.node_types[depot] = 'D'
			tokens = data_lines[l].strip(' \n').split()
			uq.verb('l', l, 'len(tokens)', len(tokens), 'tokens', tokens)
			l += 1
			if len(tokens) != 1:
				uq.fail('Error: recgn_depot_section_content')
			depot = tokens[0]
			if int(depot) != -1:
				uq.fail('Error: recgn_depot_section_content')
		self.num_depots = len(self.depots)
		self.num_customers = self.n - self.num_depots

		if self.num_depots != 1:
			uq.fail('Error: recgn_depot_section_content: ' + str(self.num_depots) + ' is an invalid number of depots as input')

		if self.split_depot:
			self.depots.append(self.n)

		return l


	def recgn_display_data_section_content(self, l, data_lines):
		self.display_coords = [Coords(0.0, 0.0) for i in range(self.n + self.num_extra_verts)]
		check_node_id = 1
		if self.display_data_type == 'TWOD_DISPLAY':
			if self.edge_weight_type in {'EUC_2D', 'EXPLICIT', 'GEO'}:
				for l in range(l, l + self.n):
					tokens = data_lines[l].strip(' \n').split()
					uq.verb('tokens', tokens)
					if len(tokens) != 3:
						uq.fail('Error: recgn_display_data_section_content: len(tokens) == ' + str(len(tokens)))
					node_id = int(tokens[0])
					uq.proceed_only_if_(node_id == check_node_id, 'node_id != check_node_id')
					check_node_id += 1
					self.display_coords[node_id - 1].x = float(tokens[1])
					self.display_coords[node_id - 1].y = float(tokens[2])
				l += 1
			elif self.edge_weight_type in {'FUNCTION', 'ALIEN_FUNCTION'} and self.edge_weight_format in {'EUC_2D', 'ALIEN_EUC_2D'} and self.node_coord_type  == 'TWOD_COORDS':
				for l in range(l, l + self.n - 1):
					tokens = data_lines[l].strip(' \n').split()
					uq.verb('tokens', tokens)
					if len(tokens) != 3:
						uq.fail('Error: recgn_display_data_section_content: len(tokens) == ' + str(len(tokens)))
					node_id = int(tokens[0])
					uq.proceed_only_if_(node_id == check_node_id, 'node_id != check_node_id')
					check_node_id += 1
					self.display_coords[node_id].x = float(tokens[1])
					self.display_coords[node_id].y = float(tokens[2])
				l += 1

			if self.split_depot:
				self.display_coords[self.n].x = self.display_coords[self.depots[0]].x + 4
				self.display_coords[self.n].y = self.display_coords[self.depots[0]].y - 4
		else:
			uq.fail('Error: recgn_display_data_section_content')

		return l


	def recgn_robustvrpsb_backhauls_section_content(self, l, data_lines):
		uq.proceed_only_if_(self.inst_type == 'ROBUSTVRPSB', 'Error: recgn_robustvrpsb_backhauls_section_content')
		self.prize           = [0.0 for i in range(self.n + self.num_extra_verts)]
		self.prize_deviation = [0.0 for i in range(self.n+ self.num_extra_verts)]
		if self.edge_weight_type == 'EUC_2D' or self.edge_weight_type == 'EXPLICIT':
			tokens = data_lines[l].strip(' \n').split()
			uq.verb('l', l, 'len(tokens)', len(tokens), 'tokens', tokens)
			l += 1
			if len(tokens) != 3:
				uq.fail('Error: recgn_robustvrpsb_backhauls_section_content')
			backhaul = int(tokens[0])
			while backhaul != -1:
				self.backhauls.append(backhaul - 1)
				self.node_types     [backhaul - 1] = 'B'
				self.prize          [backhaul - 1] = float(tokens[1])
				self.prize_deviation[backhaul - 1] = float(tokens[2])
				tokens = data_lines[l].strip(' \n').split()
				uq.verb('l', l, 'len(tokens)', len(tokens), 'tokens', tokens)
				l += 1
				if len(tokens) != 3:
					uq.fail('Error: recgn_robustvrpsb_backhauls_section_content')
				backhaul = int(tokens[0])
			for i in range(self.n + self.num_extra_verts):
				if self.node_types[i] == 'C':
					self.linehauls.append(i)
					self.node_types[i] = 'L'
		else:
			uq.fail('Error: recgn_robustvrpsb_backhauls_section_content')
		return l


	def calc_dists(self):
		if self.edge_weight_type == 'EUC_2D' or \
		self.edge_weight_type == 'FUNCTION' and  self.edge_weight_format == 'EUC_2D' or \
		self.edge_weight_type == 'ALIEN_FUNCTION' and self.edge_weight_format == 'ALIEN_EUC_2D':
			for i in range(0, (self.n + self.num_extra_verts) - 1):
				for j in range(i + 1, (self.n + self.num_extra_verts)):
					self.ds[i][j] = uq.dist(self.coords[i].x, self.coords[i].y, self.coords[j].x, self.coords[j].y,
					self.int_dist_measure,
					self.num_dec_places_real_dist)
					self.ds[j][i] = self.ds[i][j]

		elif self.edge_weight_type == 'GEO':
			for i in range(0, (self.n + self.num_extra_verts) - 1):
				for j in range(i + 1, (self.n + self.num_extra_verts)):
					# latitude === x and longitude === y, but the intuition is exchanged
					# i
					lat_i  = uq.geocoord_to_rad(self.coords[i].x)
					long_i = uq.geocoord_to_rad(self.coords[i].y)
					# j
					lat_j  = uq.geocoord_to_rad(self.coords[j].x)
					long_j = uq.geocoord_to_rad(self.coords[j].y)

					q1 = math.cos(long_i - long_j)
					q2 = math.cos(lat_i  - lat_j )
					q3 = math.cos(lat_i  + lat_j )

					self.ds[i][j] = int(uq.RRR * math.acos(0.5 * ((1.0 + q1) * q2 - (1.0 - q1) * q3)) + 1.0)
					self.ds[j][i] = self.ds[i][j]

					# uq.log_(str(i) + ' = (' + str(self.coords[i].x) + ',' + str(self.coords[i].y) + ')\t' + str(j) + ' = (' + str(self.coords[j].x) + ',' + str(self.coords[j].y) + ')\t(' + str(i) + ', ' + str(j) + '): ' + str(self.ds[i][j]))


	def calc_intern_data(self):
		uq.verb('+ ' + self.calc_intern_data.__name__)

		if self.inst_type in {'CVRP', 'ROBUSTVRPSB'}:
			if self.gcd_ccd > 1:
				uq.fail_if_(self.Q % self.gcd_ccd > 0, 'Error: calc_intern_data: Q % gcd_ccd = ' + str(self.Q % self.gcd_ccd) + ' > 0')
				self.Q = float(self.Q // self.gcd_ccd)

				for i in range(1, self.n):
					uq.fail_if_(self.qs[i] % self.gcd_ccd > 0, 'Error: calc_intern_data: self.qs[' + str(i) + '] % gcd_ccd = ' + str(self.qs[i] % self.gcd_ccd) + ' > 0')
					self.qs[i] = float(self.qs[i] // self.gcd_ccd)

			elif self.gcd_ccd < 1:
				uq.fail('Error: calc_intern_data: gcd_ccd = ' + str(self.gcd_ccd))

			self.sum_qs = 0
			self.smaller_cust_dem = math.inf
			self.larger_cust_dem = 0.0

			for i in range(1, self.n):
				self.sum_qs += self.qs[i]
				self.smaller_cust_dem = min(self.smaller_cust_dem, self.qs[i])
				self.larger_cust_dem = max(self.larger_cust_dem, self.qs[i])

			self.freq_distinct_cust_dems = dict(collections.Counter(np.array(self.qs[1:]).astype(int).tolist()))
			self.num_distinct_cust_dems = len(self.freq_distinct_cust_dems)

			self.frac_lb_k            = self.sum_qs / self.Q
			self.ceil_lb_k            = math.ceil(self.frac_lb_k)
			self.tight                = self.sum_qs / (max(self.k, self.ceil_lb_k) * self.Q)
			self.canonical_route_dist = self.get_canonical_route_dist()

			self.are_cap_n_cust_dems_ints = True
			for i in range(1, self.n):
				self.are_cap_n_cust_dems_ints = self.are_cap_n_cust_dems_ints and self.qs[i].is_integer()
			self.are_cap_n_cust_dems_ints = self.are_cap_n_cust_dems_ints and self.Q.is_integer()

			if self.are_cap_n_cust_dems_ints:
				self.gcd_ccd_calc = functools.reduce(math.gcd, np.array(self.qs[1:]).astype(int).tolist())

		elif self.inst_type in {'TSP'}:
			self.frac_lb_k = 1.0
			self.ceil_lb_k = math.ceil(self.frac_lb_k)
			self.canonical_route_dist = self.get_canonical_route_dist()

		uq.verb('intern_data: sum_qs ' + str(self.sum_qs) + ' frac_lb_k ' + str(self.frac_lb_k) + ' ceil_lb_k ' + str(self.ceil_lb_k))
		uq.verb('- ' + self.calc_intern_data.__name__)


	def calc_and_check_extern_data(self, extern_k=0, override_inst_k=True, extern_rho=0.0, extern_q0=0.0, override_inst_q0=True, extern_rho_rounding_mode=None):
		if self.inst_type in {'CVRP', 'CMVRP', 'ROBUSTVRPSB'}:
			if self.k == 0 or override_inst_k:
				if extern_k > 0:
					self.k = extern_k
				else:
					self.k = self.ceil_lb_k

			if self.qs[0] == 0.0 or override_inst_q0:
				if extern_rho > 0.0:
					if extern_rho_rounding_mode is 'noround':
						self.qs[0] = self.Q * extern_rho * self.gcd_ccd
					elif extern_rho_rounding_mode == 'floor':
						self.qs[0] = math.floor(self.Q * extern_rho * self.gcd_ccd)
					elif extern_rho_rounding_mode == 'nint':
						self.qs[0] = uq.nint(self.Q * extern_rho * self.gcd_ccd)
					elif extern_rho_rounding_mode is not None:
						uq.fail('Error: Invalid extern_rho_rounding_mode = ' + str(extern_rho_rounding_mode))
				elif extern_q0 > 0.0:
					self.qs[0] = extern_q0

		elif self.inst_type in {'TSP'}:
			self.k = 1


	def fill_discrete(self):
		if self.inst_type in {'CVRP', 'ROBUSTVRPSB'}:
			self.discrete_Q = uq.nint(self.Q)
			self.discrete_qs = [0 for i in range(self.n + self.num_extra_verts)]
			self.discrete_qs[0] = sys.maxsize  # !< there is no discrete correspondence to qs[0] # TODO: this is about to change
			if self.split_depot:
				self.discrete_qs[self.n] = 0

			self.discrete_sum_qs = 0
			for i in range(1, self.n + self.num_extra_verts):
				self.discrete_qs[i] = uq.nint(self.qs[i])
				self.discrete_sum_qs += self.discrete_qs[i]


	def get_canonical_route_dist(self):
		z = 0.0
		uq.verb(str(0))
		for i in range(1, self.n):
			uq.verb(str(i))
			z += self.ds[i - 1][i]
		uq.verb(str(0))
		z += self.ds[self.n - 1][0]
		return z


	def adjust_display_coords(self):
		if len(self.display_coords) > 0:
			max_display_coords = Coords(-float(sys.maxsize), -float(sys.maxsize))
			min_display_coords = Coords( float(sys.maxsize),  float(sys.maxsize))

			for u in range(self.n):
				max_display_coords.x = max(max_display_coords.x, self.display_coords[u].x)
				max_display_coords.y = max(max_display_coords.y, self.display_coords[u].y)

				min_display_coords.x = min(min_display_coords.x, self.display_coords[u].x)
				min_display_coords.y = min(min_display_coords.y, self.display_coords[u].y)

			interval = Coords(max_display_coords.x - min_display_coords.x, max_display_coords.y - min_display_coords.y)
			ratio = min(1.0, 16.0 / abs(interval.x), 16.0 / abs(interval.y))
			for u in range(self.n):
				self.display_coords[u].x *= ratio
				self.display_coords[u].y *= ratio


	def as_dat_str(self):
		if self.split_depot:
			return ''
		else:
			inst_as_dat_str = 'id = ' + '"{0}"'.format(self.inst_id) + ';\n' \
			                + 'n  = ' + '{:d}'.format(self.n)        + ';\n' \
			                + 'Q  = ' +           str(self.Q)        + ';\n' \
			                + 'k  = ' + '{:d}'.format(self.k)        + ';\n'

			if self.inst_type != 'TSP':
				inst_as_dat_str += 'q  =  ['
				for i in range(self.n):
					inst_as_dat_str += str(self.qs[i])
					if i != self.n - 1:
						inst_as_dat_str += ', '
				inst_as_dat_str += '];\n'

			inst_as_dat_str += 'd  = ['
			for i in range(0, self.n):
				if i == 0:
					inst_as_dat_str += '['
				else:
					inst_as_dat_str += '      ['

				for j in range(0, self.n):

					if j != 0:
						inst_as_dat_str += ', '

					inst_as_dat_str += str(self.ds[i][j])

				inst_as_dat_str += ']'
				if i != self.n - 1:
					inst_as_dat_str += ',\n'
			inst_as_dat_str += '];\n'

			return inst_as_dat_str


	def as_str(self):
		# TODO: this function must be improved if needed

		if self.split_depot:
			return ''
		else:
			inst_as_str = 'id = ' + '"{0}"'.format(self.inst_id) + '\n' \
			            + 'n  = ' + '{:d}'.format(self.n)        + '\n' \
			            + 'Q  = ' +           str(self.Q)        + '\n' \
			            + 'k  = ' + '{:d}'.format(self.k)        + '\n'

			if self.inst_type != 'TSP':
				inst_as_str += 'q  =  ['
				for i in range(self.n):
					inst_as_str += str(self.qs[i])
					if i != self.n - 1:
						inst_as_str += ', '
				inst_as_str += ']\n'

			# inst_as_str += 'd  = ['
			# for i in range(0, self.n):

			#	if i == 0:
			#		inst_as_str += '['
			#	else:
			#		inst_as_str += '      ['

			#	for j in range(0, self.n):

			#		if j != 0:
			#			inst_as_str += ', '

			#		inst_as_str += str(self.ds[i][j])

			#	inst_as_str += ']'
			#	if i != self.n - 1:
			#		inst_as_str += ',\n'
			# inst_as_str += '];\n'

			inst_as_str += 'are_cap_n_cust_dems_ints: ' + str(self.are_cap_n_cust_dems_ints) + '\n' \
						+  'gcd_ccd: ' + str(self.gcd_ccd) + '\n' \
						+  'gcd_ccd_calc: ' + str(self.gcd_ccd_calc) + '\n' \
						+  'canonical_route_dist: ' + str(self.get_canonical_route_dist())

			return inst_as_str


	# def as_yaml_str(self):
	#	return ''
	#	inst_as_yaml_str  = 'inst_id         : ' + self.inst_id            + '\n' \
	#	                  + 'comment         : ' + self.comment            + '\n' \
	#	                  + 'inst_type       : ' + self.inst_type          + '\n' \
	#	                  + 'edge_weight_type: ' + self.edge_weight_type   + '\n' \
	#	                  + 'n (dimension)   : ' + str(self.n)             + '\n' \
	#	                  + 'num_customers   : ' + str(self.num_customers) + '\n' \
	#	                  + 'sum_qs          : ' + str(self.sum_qs)        + '\n' \
	#	                  + 'Q (capacity)    : ' + str(self.Q)             + '\n' \
	#	                  + 'linf_k          : ' + str(self.linf_k)        + '\n' \
	#	                  + 'linf_min_k      : ' + str(self.linf_min_k)    + '\n' \
	#	                  + 'k               : ' + str(self.k)             + '\n' \
	#	                  + 'nodes: [\n'
	#	for i in self.nodes:
	#		inst_as_yaml_str += '    ' + str(i)
	#		if i != self.nodes[len(self.nodes) - 1]:
	#			inst_as_yaml_str += ',\n'
	#	inst_as_yaml_str += ']\n'
	#
	#	inst_as_yaml_str += 'dist: [\n'
	#
	#	inst_as_yaml_str += '    #'
	#	for j in range(len(self.nodes)):
	#		inst_as_yaml_str += '{:3d}'.format(j)
	#		if j != len(self.nodes) - 1:
	#			inst_as_yaml_str += '  '
	#	inst_as_yaml_str += '\n'
	#
	#	for idx, i in enumerate(self.nodes):
	#		inst_as_yaml_str += '    ['
	#		for j in self.nodes:
	#			if j != self.nodes[0]:
	#				inst_as_yaml_str += ', '
	#			inst_as_yaml_str += '{:3d}'.format(self.ds[i][j])
	#		inst_as_yaml_str += ']'
	#		if i != self.nodes[len(self.nodes) - 1]:
	#			inst_as_yaml_str += ',  # ' + '{:3d}'.format(idx) + '\n'
	#	inst_as_yaml_str += ']  # ' + '{:3d}'.format(idx)
	#
	#	return inst_as_yaml_str
